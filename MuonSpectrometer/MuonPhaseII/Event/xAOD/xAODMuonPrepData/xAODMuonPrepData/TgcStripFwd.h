/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODMUONPREPDATA_TgcStripFWD_H
#define XAODMUONPREPDATA_TgcStripFWD_H
/** @brief Forward declaration of the xAOD::TgcStrip */
namespace xAOD{
   class TgcStrip_v1;
   using TgcStrip = TgcStrip_v1;

   class TgcStripAuxContainer_v1;
   using TgcStripAuxContainer = TgcStripAuxContainer_v1;
}
#endif
