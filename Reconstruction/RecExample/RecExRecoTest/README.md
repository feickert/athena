RecExRecoTest
========================

Various domain-targeted tests of ATLAS reconstruction (egamma, muon, tau, JetETMiss, Particle Flow, Tracking, Calorimeter).
Python scripts setup all necessary flags, and rely on domain-specific configurations where possible.
Bash scripts run both data and MC checks, and are run as part of the nightly ART tests.
Learn more at https://twiki.cern.ch/twiki/bin/view/AtlasComputing/RecoRecipes
For any issues, file a ticket at https://its.cern.ch/jira/projects/ATLASRECTS/